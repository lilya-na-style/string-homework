package test;

import main.StringsHW;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StringsHWTest {


    private StringsHW cut = new StringsHW();


    static Arguments[] getStringFromIntArgs(){
        return new Arguments[]{
                Arguments.arguments(59,"59"),
                Arguments.arguments(4,"4"),
                Arguments.arguments(22,"22")

        };
    }
    @ParameterizedTest
    @MethodSource("getStringFromIntArgs")
    void getStringFromIntTest(int i, String str) {
        String result =cut.getStringFromInt(i, str);
        Assertions.assertEquals(str, result);
    }


    static Arguments[] getStringFromDoubleArgs(){
        return new Arguments[]{
                Arguments.arguments(66.6,"66.6"),
                Arguments.arguments(10.0,"10.0"),
                Arguments.arguments(-7.77,"-7.77")

        };
    }
    @ParameterizedTest
    @MethodSource("getStringFromDoubleArgs")
    void getStringFromDoubleTest(double d, String str) {
        String result =cut.getStringFromDouble(d, str);
        Assertions.assertEquals(str, result);
    }


    static Arguments[] getIntFromStringArgs(){
        return new Arguments[]{
                Arguments.arguments("7", 7),
                Arguments.arguments("404", 404),
                Arguments.arguments("-67", -67)

        };
    }
    @ParameterizedTest
    @MethodSource("getIntFromStringArgs")
    void getIntFromStringTest(String str, int i) {
        int result = cut.getIntFromString(str, i);
        Assertions.assertEquals(i, result);
    }

    static Arguments[] getDoubleFromStringArgs(){
        return new Arguments[]{
                Arguments.arguments("7.7", 7.7),
                Arguments.arguments("404.44", 404.44),
                Arguments.arguments("67.0", 67.0)

        };
    }
    @ParameterizedTest
    @MethodSource("getIntFromStringArgs")
    void getDoubleFromStringTest(String str, double d) {
        double result = cut.getDoubleFromString(str, d);
        Assertions.assertEquals(d, result);
    }

    static Arguments[] getTheShortestWordArgs(){
        return new Arguments[]{
                Arguments.arguments("how u doin", "u"),
                Arguments.arguments("is anybody here", "is"),
                Arguments.arguments("i like it", "i")

        };
    }
    @ParameterizedTest
    @MethodSource("getTheShortestWordArgs")
    void getTheShortestWordTest(String str, String word) {
        String result = cut.getTheShortestWord(str, word);
        Assertions.assertEquals(word, result);
    }

    static Arguments[] changeWordSymbolsArgs(){
        return new Arguments[]{
                Arguments.arguments("hello how are u doin", "he$$$"),
                Arguments.arguments("its a good day", "g$$$"),
                Arguments.arguments("i like it", "l$$$")

        };
    }
    @ParameterizedTest
    @MethodSource("changeWordSymbolsArgs")
    void changeWordSymbolsTest(String words, String word) {
        String result = cut.changeWordSymbols(words, word);
        Assertions.assertEquals(word, result);
    }

    static Arguments[] addSpacesArgs(){
        return new Arguments[]{
               Arguments.arguments("yes!its true", "yes! its true"),
               Arguments.arguments("really!are u sure?its weird", "really! are u sure? its weird"),

        };
    }
    @ParameterizedTest
    @MethodSource("addSpacesArgs")
    void addSpacesTest(String s, String newS) throws IOException {
        String result = cut.addSpaces(s, newS);
        Assertions.assertTrue(newS.equals(result));
    }


    static Arguments[] getUniqueWordsStringArgs(){
        return new Arguments[]{
                Arguments.arguments("that is a wonderful day this day is best of all", " thaiswonderfulyb"),

        };
    }

    @ParameterizedTest
    @MethodSource("getUniqueWordsStringArgs")
    void getUniqueWordsStringTest(String s, String expected){
        String result = cut.getUniqueWordsString(s, expected);
        Assertions.assertEquals(expected, result);
    }



    static Arguments[] countWordsArgs() {
        return new Arguments[]{
                Arguments.arguments(1, "one"),
                Arguments.arguments(3, "two three four"),

        };
    }
    @ParameterizedTest
    @MethodSource("countWordsArgs")
    void getCountOfWordsTest(int expected, String line){
        int result = cut.countWords(line);
        Assertions.assertEquals(expected, result);
    }



    static Arguments[] removePartOfStringArgs(){
        return new Arguments[]{
                Arguments.arguments( "hi how its going there",  6, 22 ,"hi hoits going there")

        };
    }

    @ParameterizedTest
    @MethodSource("removePartOfStringArgs")
    void cutSubstringFromLineTest(String s, int index, int strLength, String newStr){
        String result = cut.removePartOfString(s, index, strLength, newStr);

        Assertions.assertEquals(newStr, result);
    }









}
