package main;

import java.io.IOException;

public class StringsHW {

 //#1
   public static void getUpperAlphabetString(){
        for(char i = 'a'; i <= 'z'; i++)
        {
            String alphChars = Character.toString(i) + " ";
            System.out.print(alphChars.toUpperCase());

        }
}

    public static void getReverseAlphabetString(){
        for(char i = 'Z'; i >= 'A'; i--)
        {
            String reverseChars = Character.toString(i) + " ";
            System.out.print(reverseChars.toLowerCase());

        }
    }

    public static void getRussianAlphabetString(){
        for(char i = 'а'; i <= 'я'; i++)
        {
            String rusChars = Character.toString(i) + " ";
            System.out.print(rusChars.toLowerCase());

        }
    }

    public static void getNumberString(){
        for(int i = 0; i <= 9; i++)
        {
            String numString = Integer.toString(i) + " ";
            System.out.print(numString);

        }
    }

    public static void getAsciiPrintableRange(){
        for (int c = 32; c <= 127; c++) {
            String s = (char)c + " ";
            System.out.print(s);
        }
    }


    //#2

    public static String getStringFromInt(int i, String str){
        str = Integer.toString(i);
        System.out.println(str);
        return str;
    }


    public static String getStringFromDouble(double d, String s){
        s = Double.toString(d);
        System.out.println(s);
        return s;
    }

    public static int getIntFromString(String s, int i){
        i = Integer.valueOf(s);
        System.out.println(i);
        return i;
    }

    public static double getDoubleFromString(String s, double d){
        d = Double.parseDouble(s);
        System.out.println(d);
        return d;
    }

    //#3

    public static String getTheShortestWord(String s, String shortestWord) {
        String str = s + " ";
        char ch = ' ';
        int len = str.length(), l = 0;
        int min = len, max = 0;
        shortestWord = "";
        String  longestWord = "", word = "";
        for (int i = 0; i < len; i++) {
            ch = str.charAt(i);
            if (ch != ' ') {
                word += ch;
            }                                     //if ends
            else {
                l = word.length();
                if (l < min) {
                    min = l;
                    shortestWord = word;
                }                                     //if ends
                if (l > max) {
                    max = l;
                    longestWord = word;
                }
                word = "";
            }
        }
        System.out.println("Shortest word = " + shortestWord + " with length " + min);
       // System.out.println("Longest word = " + longestWord + " with length " + max);
        return shortestWord;
    }
    public static String smallest(String words[]) {
        if (words == null || words.length < 1) {
            return "";
        }
        String smallest = words[0];
        for (int i = 1; i < words.length; i++) {
            if (words[i].length() < smallest.length()) {
                smallest = words[i];
            }
        }
        System.out.println(smallest);
        return smallest;
    }// smallest

    public static String changeWordsSymbols(String [] words, String s){
       // if (words == null || words.length < 1) {
     //       return "";
      //  }
        s = null;
        for (int i = 1; i < words.length; i++) {
            if (words[i].length() < 4) {
                s = words[i] + "$ ";
            }System.out.print(s);
        }

        return s;
    }// smallest

    public static String changeWordSymbols(String s, String w) {
        int i = 0;
        String [] mass = s.split(" ");
        for (i = 0; i < mass.length-1; i++) {
            //сопоставление слова и ключа
            if (mass[i].length() >= 4) {
                String substring = mass[i].substring(0, mass[i].length() - 3);
                mass[i] = substring + "$$$";
                 w = mass[i];
                //System.out.print(mass[i]);
            }
        }
        //вывод результата
       // for (i = 0; i < mass.length; i++) {

      //  }

        return w;
    }

    public static String addSpaces(String s, String result) throws IOException {
        String delimiters = ".,?!:;";

        result = s.replaceAll("[.,!?:]", "$0 ");
        //String result = s.replaceAll("[.!?\\-]", "$0 ");
        System.out.println (result);
        return result;

    }


    public static String getUniqueWordsString(String s, String unique){
        unique = " ";
        char[] arr = s.toCharArray();
        for(int i = 0; i < arr.length; i++){
            if(!unique.contains(String.valueOf(arr[i]))){
                unique = unique + arr[i];

            }
        } System.out.println(unique);
        return unique;
        
    }


    public int countWords(String s){
        int count = 0;
        String[] userWords = s.trim().split("\\s+");
        for(int i = 0; i < userWords.length; i++){
            if(!userWords[i].matches(".*[^a-zA-Z]+.*")) {
                count++;
            }
        }
        return count;
    }

    public static String removePartOfString(String s, int index, int strLength, String newStr){

   newStr = s.substring(0, strLength);

        if(s.length() >= index + strLength){

        newStr = newStr + s.substring(index + strLength);
            System.out.println(newStr);
    }
        return newStr;
        
}

    public static String reverseString(String s){
        String newStr = "";
        char[] arr = s.toCharArray();
        char[] reversedStr = new char[arr.length];
        for (int i = 0; i <arr.length ; i++) {
            reversedStr[i] = arr[arr.length-i-1];
            newStr = String.valueOf(reversedStr);

        }

        System.out.println(newStr);
        return newStr ;
    }


    public static String removeLastWord(String s){
        String trimedStr = s.trim();
        String newStr = trimedStr.substring(0, trimedStr.lastIndexOf(" "));
        System.out.println(newStr);
        return newStr;
    }




       public static void main(String[] args) throws IOException {
        getUpperAlphabetString();
        System.out.println();
        getReverseAlphabetString();
        System.out.println();
        getRussianAlphabetString();
        System.out.println();
        getNumberString();
        System.out.println();
        getAsciiPrintableRange();
        System.out.println();
       // getStringFromInt(66667);
        getStringFromDouble(66.6, "66.6");
        getIntFromString("111", 111);
        getDoubleFromString("56.4", 56.4);
        getTheShortestWord("hello how are u doin", "u");
        smallest(new String[]{"llo", "how", "doin"});
        changeWordsSymbols(new String[]{"hello", "how", "are", "u", "doin"}, "how$ are$ u$");
        System.out.println();
        changeWordSymbols("hello how are u doin", "he$$$ d$$$");
        System.out.println();
        addSpaces("hi!how u doin,dudes?is anybody here?", "hi! how u doin, dudes? is anybody here?");
        System.out.println();
        getUniqueWordsString("that is a wonderful day this day is best of all", "thaiswonderfulyb");
        System.out.println();
        removePartOfString("hi how its going there", 2, 5, "hi hoits going there");
        reverseString("what is the date today");
        removeLastWord("how its going friends");







    }
}
